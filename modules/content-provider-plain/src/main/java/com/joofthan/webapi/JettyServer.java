package com.joofthan.webapi;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;

public class JettyServer {
    private Server server;

    public void start() throws Exception {
        server = new Server();
        ServerConnector connector = new ServerConnector(server);
        int port;
        try {
            port = Integer.parseInt(System.getenv("PORT"));
        }catch (NumberFormatException e){
            port = 8080;
        }
        connector.setPort(port);
        server.setConnectors(new Connector[]{connector});
        var handler = new ServletHandler();
        server.setHandler(handler);
        handler.addServletWithMapping(RestApi.class, "/");
        server.start();
        System.out.println("Running on http://localhost:"+port);
    }
}