package com.joofthan.webapi;

import com.joofthan.cdi.enterprise.json.JSON;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;


public class RestApi extends HttpServlet {

    protected void doGet(
            HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {

        // var recorder = AutomaticTvRecorderFactory.createDefaultConfig();
        // recorder.run();
        // recorder.shutdown();

        //Browser browser = new Browser();

        response.setContentType("application/json");
        response.setStatus(HttpServletResponse.SC_OK);
        //String result = browser.favorites().youTv().getTitle();
        var json = getJson("result");

        //if (true) throw new RuntimeException("Test exi");
        response.getWriter().println(json);

        //browser.close();
    }

    private JSON getJson(String result) {
        return new JSON()
                .with("TestEntity", new JSON()
                        .with("status", result)
                        .with("alter", 12.3)
                )
                .with("hello", false)
                .with("subsub", new JSON()
                        .with("Sub1", new JSON()
                                .with("house", new JSON()
                                        .with("bath", false)
                                        .with("kitchen", new JSON()
                                                .with("stove", true)
                                                .with("microwave", true)
                                        )
                                        .with("window", true)
                                        .with("alter", 4)
                                )
                                .with("alter2", 12.32)
                        )
                );
    }
}