package com.joofthan.web.browser.pages.ardmediathek;

import com.joofthan.web.browser.Browser;
import com.joofthan.web.browser.alternative.Page;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ArdMediathekHomeTest {

    private static Browser browser;
    private ArdMediathekHome page;

    @BeforeAll
    static void setupBrowser(){
        browser = new Browser();
    }

    @BeforeEach
    void setupPage(){
        page = browser.favorites().ardMediathek();
    }

    @AfterAll
    static void teardown(){
        browser.close();
    }

    @Test
    @Disabled
    void testSearch(){
        var res = page.search("Babylon");
        assertEquals("Babylon Berlin", res.get(0).getTitle());
    }

    @Test
    void testSearchAlternative(){
        var page = new Page("https://www.ardmediathek.de/ard/suche/");
        page.type("input", "test");
        page.click("#search");


        assertEquals("Babylon Berlin", page.getTitle());
    }
}