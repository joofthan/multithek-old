package com.joofthan.web.browser.pages.ardmediathek;

import com.joofthan.web.browser.Browser;
import com.joofthan.web.browser.PageObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

public class ArdMediathekHome extends PageObject {

    public ArdMediathekHome(Browser browser) {
        super(browser);
    }

    public List<SearchResult> search(String text) {
        loadUrl("https://www.ardmediathek.de/ard/suche/");
        typeReplace(By.tagName("input"), text);
        var elem = waitForElement(By.className("swiper-wrapper"));
        var searchResultElements = elem.findElements(By.className("swiper-slide"));

        var searchResults = new ArrayList<SearchResult>();

        for (var element:searchResultElements) {
            var title = element.findElement(By.tagName("h3")).getText();
            searchResults.add(new SearchResult(title));
        }

        return searchResults;
    }

    @Getter
    @AllArgsConstructor
    public static class SearchResult {
        private final String title;
    }
}
