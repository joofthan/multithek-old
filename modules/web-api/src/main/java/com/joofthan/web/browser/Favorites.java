package com.joofthan.web.browser;

import com.joofthan.web.browser.pages.ardmediathek.ArdMediathekHome;
import com.joofthan.web.browser.pages.joofthan.JoofthanHome;
import com.joofthan.web.browser.pages.youtv.YouTvHome;

public class Favorites {
    private final Browser browser;

    public Favorites(Browser browser){
        this.browser = browser;
    }

    public YouTvHome youTv(){
        return new YouTvHome(browser.initWebDriver("https://www.youtv.de/"));
    }

    public JoofthanHome joofthan(){
        return new JoofthanHome(browser.initWebDriver("http://www.joofthan.com/"));
    }

    public ArdMediathekHome ardMediathek() {
        return new ArdMediathekHome(browser.initWebDriver("https://www.ardmediathek.de/"));
    }
}
