package com.joofthan.web.browser.pages.youtv;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.joofthan.web.browser.Browser;

import java.time.LocalDateTime;
import java.util.List;

public class YouTvTvProgram extends BaseFrame {

    public YouTvTvProgram(Browser driver) {
        super(driver);
    }

    @Deprecated
    public YouTvTvProgram clickSendung(String channelName, LocalDateTime startTime, LocalDateTime endTime) {
        List<WebElement> channelElements = waitForElement(By.className("tvguide-channels")).findElements(By.tagName("li"));
        int targetRow = -1;
        for(int i = 0; i< channelElements.size();i++){
            if(channelName.equals(channelElements.get(i).findElement(By.tagName("img")).getAttribute("alt").toUpperCase())){
                targetRow = i;
                break;
            }
        }
/*
        List <WebElement> rowElements = waitForElement(By.className("tvguide-broadcasts-wrapper")).findElements(By.tagName("li"));
        System.out.println(rowElements.size());
        WebElement elem = rowElements.get(targetRow);
        List<WebElement> sendungen = elem.findElements(By.tagName("li"));
        System.out.println(sendungen.size());
        for(WebElement sendung:sendungen){
            System.out.println(sendung.findElement(By.tagName("small")).getText());
        }
*/
        return this;
    }
}
