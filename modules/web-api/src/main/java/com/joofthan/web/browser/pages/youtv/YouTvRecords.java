package com.joofthan.web.browser.pages.youtv;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import com.joofthan.web.browser.Browser;

import java.util.List;

public class YouTvRecords extends BaseFrame{

    private By broadcastTable = By.className("broadcasts-table");

    public YouTvRecords(Browser driver) {
        super(driver);
    }

    public int zeilenAnzahl() {
        try{
            return waitForElement(broadcastTable).findElements(By.className("action-play")).size();
        }catch (TimeoutException e){
            return 0;
        }


    }

    public Row zeile(int number) {
        return new Row(number);
    }

    public class Row{
        private final int rowNumber;

        Row(int rowNumber){
            this.rowNumber = rowNumber;
        }

        public String getTitle(){
            List<WebElement> elements = waitForElement(broadcastTable).findElements(By.className("broadcasts-table-cell-title"));

            return elements.get(rowNumber+1).getText();
        }

        public YouTvDetails clickTitle(){
            List<WebElement> elements = waitForElement(broadcastTable).findElements(By.className("broadcasts-table-cell-title"));
            elements.get(rowNumber+1).findElement(By.tagName("a")).click();

            return new YouTvDetails(browser);
        }

        public YouTvStream clickPlay(){
            List<WebElement> elements = waitForElement(broadcastTable).findElements(By.className("action-play"));
            elements.get(rowNumber).click();

            return new YouTvStream(browser);
        }
    }
}
