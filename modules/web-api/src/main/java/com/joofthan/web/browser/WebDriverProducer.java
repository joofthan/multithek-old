package com.joofthan.web.browser;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class WebDriverProducer {
    public static WebDriver produceWebDriver() {
        WebDriver driver;
        try{
            driver = tryCreateChromeDriver();
        }catch (Exception e){
            try{
                driver = tryCreateEdgeDriver();
            } catch (Exception e2){
                try{
                    driver = new FirefoxDriver();
                } catch (Exception e3){
                    e.printStackTrace();
                    e2.printStackTrace();
                    e3.printStackTrace();
                    throw new RuntimeException(e.getMessage() + "-----------------\n"+e2.getMessage() +"-----------------\n"+e3.getMessage());
                }
            }
        }

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return driver;
    }

    private static WebDriver tryCreateEdgeDriver() {
        WebDriver driver;
        try{
            System.setProperty("webdriver.edge.driver", "modules\\webbrowserDriver\\msedgedriver.exe");
            driver = new EdgeDriver();
        }catch (Exception e){
            try{
                System.setProperty("webdriver.edge.driver", "..\\webbrowserDriver\\msedgedriver.exe");
                driver = new EdgeDriver();
            }catch(Exception e2){
                System.setProperty("webdriver.edge.driver", "..\\modules\\webbrowserDriver\\msedgedriver.exe");
                driver = new EdgeDriver();
            }
        }
        return driver;
    }

    private static WebDriver tryCreateChromeDriver() {
        try{
            return new ChromeDriver();
        } catch (Exception e){
            //Heroku
            WebDriver driver;
            System.setProperty("webdriver.chrome.driver", "chromedriver ");
            ChromeOptions options = new ChromeOptions();
            options.setBinary(System.getenv("GOOGLE_CHROME_BIN"));
            driver = new ChromeDriver();
            return driver;
        }

    }
}
