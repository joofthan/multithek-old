package com.joofthan.web.browser.pages.youtv;

import org.openqa.selenium.By;
import com.joofthan.web.browser.Browser;

public class YouTvDetails extends BaseFrame{
    public YouTvDetails(Browser driver) {
        super(driver);
    }

    public Integer getYear(){
        String placeAndYear = waitForElement(By.className("broadcast-content")).findElement(By.tagName("b")).getText();
        String yearString = placeAndYear.substring(placeAndYear.indexOf("(")+1, placeAndYear.indexOf(")"));

        return Integer.valueOf(yearString);
    }

    public void clickBack(){
        waitForElement(By.className("broadcast-header")).findElement(By.className("close")).click();
        waitSeconds(1);
    }
}
