package com.joofthan.web.browser.alternative;

import com.joofthan.web.browser.WebDriverProducer;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Page {
    private WebDriver driver;

    public Page(String url){
        initWebDriver(url);
    }

    private Page initWebDriver(String url){
        if(driver == null){
            driver = WebDriverProducer.produceWebDriver();
        }
        driver.manage().window().setPosition(new Point(-1000,-1000));
        driver.get(url);

        return this;
    }

    public void close(){
        if(driver != null){
            driver.close();
            driver = null;
        }
    }

    private WebDriver getDriver() {
        if(driver == null){
            throw new RuntimeException("Driver is null. initWebDriver() was not called.");
        }

        return driver;
    }


    public void loadUrl(String url){
        driver.get(url);
    }

    public String getTitle(){
        return driver.getTitle();
    }

    public Page typeReplace(String byCondition, String text){
        //TODO: strg + a
        return type(byCondition, text);
    }

    public Page type(String byCondition, String text){
        WebElement element = waitForElement(byCondition);
        element.sendKeys(text);

        return this;
    }

    public void click(String byCondition){
        waitForElement(byCondition).click();
    }

    public void pressEnterOn(String byCondition){
        WebElement element = waitForElement(byCondition);
        pressEnterOn(element);
    }

    public void pressEnterOn(WebElement element){
        element.sendKeys(Keys.RETURN);
    }

    private WebElement waitForElement(String byCondition){
        return waitForElement(byCondition, 10);
    }
    private WebElement waitForElement(String selector, int timeOutInSeconds){
        WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(selector)));

        return element;
    }

    public boolean elementExists(String byCondition){
        try{
            waitForElement(byCondition, 1);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void waitSeconds(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
