package com.joofthan.web.browser.pages.youtv;

import org.openqa.selenium.By;
import com.joofthan.web.browser.Browser;

public class YouTvStream extends BaseFrame{
    public YouTvStream(Browser driver) {
        super(driver);
    }

    public String getVideoUrl() {
        String videoUrl = waitForElement(By.id("youtv-video")).getAttribute("src");

        return videoUrl;
    }
}
