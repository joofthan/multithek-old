package com.joofthan.web.browser.pages.youtv;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import com.joofthan.web.browser.Browser;

import java.util.List;

public class YouTvSearch extends BaseFrame{
    private By broadcastTable = By.className("broadcast-table");

    public YouTvSearch(Browser driver) {
        super(driver);
    }

    public int zeilenAnzahl() {
        try{
            List<WebElement> elements = waitForElement(broadcastTable).findElements(By.tagName("tr"));
            return elements.size() - 1;
        } catch (TimeoutException e){
            return 0;
        }
    }

    public Row zeile(int number) {
        return new Row(number + 1);
    }

    public int seitenAnzahl() {
        List<WebElement> pageLiElements = waitForElement(By.className("pagination")).findElements(By.className("selectable"));
        return pageLiElements.size();
    }

    public void clickSeite(int seitenIndex) {
        List<WebElement> pageLiElements = waitForElement(By.className("pagination")).findElements(By.className("selectable"));
        pageLiElements.get(seitenIndex).click();
    }

    public class Row{
        private final int rowNumber;

        Row(int rowNumber){
            this.rowNumber = rowNumber;
        }

        public String getTitle(){
            List<WebElement> elements = waitForElement(broadcastTable).findElements(By.tagName("tr"));

            return elements.get(this.rowNumber).findElements(By.tagName("td")).get(0).getText();
        }

        public Row clickRecord(){
            List<WebElement> elements = waitForElement(broadcastTable).findElements(By.tagName("tr"));

            elements.get(this.rowNumber).findElement(By.className("action-record")).click();

            return this;
        }

        public boolean isMarkedAsPending() {
            List<WebElement> elements = waitForElement(broadcastTable).findElements(By.tagName("tr"));

            return elements.get(this.rowNumber).findElement(By.className("action-record")).getAttribute("data-recording").equals("true");
        }
    }
}
