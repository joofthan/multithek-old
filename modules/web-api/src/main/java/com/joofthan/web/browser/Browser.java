package com.joofthan.web.browser;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;

public class Browser {
    private WebDriver driver;
    private Favorites favorites;

    Browser initWebDriver(String url){
        if(driver == null){
            driver = WebDriverProducer.produceWebDriver();
        }
        driver.manage().window().setPosition(new Point(-1000,-1000));
        driver.get(url);

        return this;
    }

    public Favorites favorites(){
        if(favorites == null){
            favorites = new Favorites(this);
        }

        return favorites;
    }

    public void close(){
        if(driver != null){
            driver.close();
            driver = null;
        }
    }

    WebDriver getDriver() {
        if(driver == null){
            throw new RuntimeException("Driver is null. initWebDriver() was not called.");
        }

        return driver;
    }
}
