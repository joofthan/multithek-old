package com.joofthan.webapi;

import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public class TesEntity {
    String name;
    int alter;
}
